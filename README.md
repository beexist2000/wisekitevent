# 와이즈킷 이벤트 관리 API
***
와이즈킷 이벤트를 관리하는 api
***

### VERSION
```
0.0.1
```

### LANGUAGE
```
JAVA 17
SpringBoot 3.0.8
```

### 기능
```
* 유저 관리
 - 유저 생성
 - 회원 리스트 조회
 - 단일 회원 조회
 - 로그인
 
* 이벤트 관리
 - 이벤트 참가
 - 이벤트 참여 리스트 조회
 - 이벤트 참여 데이터 조회
```

### 예시
> * 스웨거 전체 화면
>
>![swagger_all](./images/swagger_all.png)

> * 스웨거 유저 관리
>
>![swagger_member](./images/swagger_member.png)

> * 스웨거 이벤트 관리
> 
> ![swagger_prize](./images/swagger_prize.png)