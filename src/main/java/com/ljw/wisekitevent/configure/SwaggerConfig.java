package com.ljw.wisekitevent.configure;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {
    private String version;

    @Bean
    public GroupedOpenApi apiV1() {
        version = "V1";

        return GroupedOpenApi.builder()
                .group(version)
                .pathsToMatch("/v1/**")
                .build();
    }

    @Bean
    public GroupedOpenApi apiV2() {
        version = "V2";

        return GroupedOpenApi.builder()
                .group(version)
                .pathsToMatch("/v2/**")
                .build();
    }

    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI()
                .components(new Components())
                .info(getApiInfo());

    }

    private Info getApiInfo() {
        String title = "와이즈킷 이벤트 관리 API";
        version = "0.0.1";

        return new Info().title(title)
                .description("Swagger API Docs")
                .version(version)
                .license(new License()
                        .name("Apache License Version 2.0")
                        .url("http://www.apache.org/licenses/LICENSE-2.0"));
    }
}
