package com.ljw.wisekitevent.service;

import com.ljw.wisekitevent.enums.ResultCode;
import com.ljw.wisekitevent.model.common.CommonResult;
import com.ljw.wisekitevent.model.common.ListResult;
import com.ljw.wisekitevent.model.common.SingleResult;
import org.springframework.stereotype.Service;

@Service
public class ResponseService {
    /**
     * 처리한 리스트형 데이터에 성공 여부를 묻고 그 정보를 저장
     *
     * @param result 처리 정보가 담길 결과값
     * @param isSuccess 성공 여부
     * @return 처리한 정보를 담은 결과 값
     * @param <T> 데이터 타입
     */
    public static <T> ListResult<T> getListResult(ListResult<T> result, boolean isSuccess) {
        if (isSuccess) setSuccessResult(result); // isSuccess가 true라면, 성공 정보를 저장
        else setFailResult(result); // 아니라면 실패 정보를 저장

        return result; // ListResult<T> 객체를 반환
    }

    /**
     * 처리한 데이터에 성공했다는 정보를 저장
     *
     * @param data 처리한 데이터
     * @return 결과 정보
     * @param <T> 데이터 타입
     */
    public static <T> SingleResult<T> getSingleResult(T data) {
        SingleResult<T> result = new SingleResult<>(); // SingleResult<T> 타입의 객체 생성
        result.setData(data); // 데이터 저장
        setSuccessResult(result); // 성공 정보 저장
        return result; // SingleResult<T> 타입의 객체 반환
    }

    /**
     * 성공 결과를 받아서 반환한다.
     *
     * @return 성공 정보가 담길 결과 값
     */
    public static CommonResult getSuccessResult() {
        CommonResult result = new CommonResult(); // 결과 정보를 담을 객체 생성
        setSuccessResult(result); // 객체에 성공 정보를 저장
        return result; // CommonResult 객체 반환
    }

    /**
     * 실패 결과를 받아서 반환한다
     *
     * @param resultCode 실패 원인이 담긴 파라미터
     * @return 실패 정보가 담길 결과 값
     */
    public static CommonResult getFailResult(ResultCode resultCode) {
        CommonResult result = new CommonResult(); // 결과 정보를 담을 객체 생성
        result.setIsSuccess(false); // 실패했다는 정보 저장
        result.setCode(resultCode.getCode()); // 실패 원인이 담긴 code 값 저장
        result.setMsg(resultCode.getMsg()); // 실패 원인이 담긴 msg 값 저장

        return result; // CommonResult 객체 반환
    }

    /**
     * 결과에 성공 정보를 담는다
     * static은 RAM에 일정 공간을 고정시켜 그 메서드를 자주 사용할 때 사용한다.
     * static은 많이 쓰면 사용할 수 있는 RAM 용량이 적어진다.
     *
     * @param result 성공 정보가 담길 결과 변수
     */
    private static void setSuccessResult(CommonResult result) {
        result.setIsSuccess(true); // 성공했다는 정보를 저장
        result.setCode(ResultCode.SUCCESS.getCode()); // 성공 정보가 담긴 code 값 저장
        result.setMsg(ResultCode.SUCCESS.getMsg()); // 성공 정보가 담긴 msg 값 저장
    }

    /**
     * 결과에 실패 정보를 담는다
     *
     * @param result 실패 정보가 담길 결과 변수
     */
    private static void setFailResult(CommonResult result) {
        result.setIsSuccess(false); // 실패했다는 정보를 저장
        result.setCode(ResultCode.FAILED.getCode()); // 실패 정보가 담긴 code 값 저장
        result.setMsg(ResultCode.FAILED.getMsg()); // 실패 정보가 담긴 msg 값 저장
    }
}
