package com.ljw.wisekitevent.service;

import com.ljw.wisekitevent.entity.Member;
import com.ljw.wisekitevent.exception.CBadLoginException;
import com.ljw.wisekitevent.exception.CDuplicatingUsernameException;
import com.ljw.wisekitevent.exception.CMissingDataException;
import com.ljw.wisekitevent.model.common.ListResult;
import com.ljw.wisekitevent.model.member.MemberLoginResponse;
import com.ljw.wisekitevent.model.member.MemberRequest;
import com.ljw.wisekitevent.model.member.MemberItem;
import com.ljw.wisekitevent.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;

    /**
     * 유저 생성
     *
     * @param request 유저 생성 정보
     */
    public void setMember(MemberRequest request) {
        if (isDuplicateUsername(request.getUsername())) throw new CDuplicatingUsernameException();

        request.setPassword(passwordEncoder.encode(request.getPassword()));
        Member member = new Member.UserBuilder(request).build();
        memberRepository.save(member);
    }

    /**
     * 중복된 아이디 체크
     *
     * @param username 유저 아이디
     * @return 아이디 중복 여부
     */
    private boolean isDuplicateUsername(String username) {
        return memberRepository.countByUsername(username) > 0;
    }

    /**
     * 로그인
     *
     * @param request 로그인 정보
     * @return 유저 시퀀스가 담긴 데이터
     */
    public MemberLoginResponse login(MemberRequest request) {
        Optional<Member> member = memberRepository.findByUsername(request.getUsername());
        if (member.isEmpty() || !passwordEncoder.matches(request.getPassword(), member.get().getPassword())) throw new CBadLoginException();

        return new MemberLoginResponse.MemberLoginResponseBuilder(member.get()).build();
    }

    /**
     * 유저 리스트 조회
     *
     * @return 유저 리스트
     */
    public ListResult<MemberItem> getMembers() {
        List<Member> members = memberRepository.findAll();
        List<MemberItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberItem item = new MemberItem.MemberItemBuilder(member).build();
            result.add(item);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 유저 시퀀스로 유저 정보 조회
     *
     * @param memberId 유저 시퀀스
     * @return 유저 정보
     */
    public MemberItem getMember(long memberId) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        return new MemberItem.MemberItemBuilder(member).build();
    }

    /**
     * 유저 정보 전체를 반환
     *
     * @param memberId 유저 시퀀스
     * @return 유저 정보 전체
     */
    public Member getMemberFull(long memberId) {
        return memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
    }
}
