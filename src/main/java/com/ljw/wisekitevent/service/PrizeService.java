package com.ljw.wisekitevent.service;

import com.ljw.wisekitevent.entity.Member;
import com.ljw.wisekitevent.entity.Prize;
import com.ljw.wisekitevent.enums.Ranking;
import com.ljw.wisekitevent.exception.CDuplicationEventDataException;
import com.ljw.wisekitevent.exception.CFullAverageException;
import com.ljw.wisekitevent.exception.CMissingDataException;
import com.ljw.wisekitevent.exception.CNotEventPeriodException;
import com.ljw.wisekitevent.model.common.ListResult;
import com.ljw.wisekitevent.model.prize.PrizeItem;
import com.ljw.wisekitevent.model.prize.PrizeResponse;
import com.ljw.wisekitevent.repository.PrizeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class PrizeService {
    private final PrizeRepository prizeRepository;

    private final LocalDateTime dateEventStart = LocalDateTime.of(2023, 12, 16, 0, 0, 0);
    private final LocalDateTime dateEventEnd = LocalDateTime.of(2023, 12, 29, 23, 59, 59);

    /**
     * 이벤트 참가
     *
     * @param member 유저 정보
     * @return 등수를 포함한 이벤트 참가 완료 메시지
     */
    public PrizeResponse setPrize(Member member) {
        if(!isPrizeDate()) throw new CNotEventPeriodException();
        if(isDuplicatedMember(member)) throw new CDuplicationEventDataException();

        int ranking = drawPrize();

        Prize prize = new Prize.PrizeBuilder(member, ranking).build();
        prizeRepository.save(prize);

        return new PrizeResponse.PrizeResponseBuilder(ranking).build();
    }

    /**
     * 이벤트 참여 리스트 조회
     *
     * @return 이벤트 참여 리스트
     */
    public ListResult<PrizeItem> getPrizes() {
        List<Prize> prizes = prizeRepository.findAll();
        List<PrizeItem> result = new LinkedList<>();

        prizes.forEach(prize -> {
            PrizeItem newItem = new PrizeItem.PrizeItemBuilder(prize).build();
            result.add(newItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 이벤트 참여 정보 조회
     *
     * @param memberId 유저 시퀀스
     * @return 이벤트 참여 정보
     */
    public PrizeItem getPrize(int memberId) {
        Prize prize = prizeRepository.findByMemberId(memberId).orElseThrow(CMissingDataException::new);

        return new PrizeItem.PrizeItemBuilder(prize).build();
    }

    /**
     * 유저가 이벤트를 이미 참가했는지 필터링
     *
     * @param member 유저 정보
     * @return 참가 여부
     */
    private boolean isDuplicatedMember(Member member) {
        return prizeRepository.countByMember(member) > 0;
    }

    /**
     * 이벤트 기간인지 필터링
     *
     * @return 이벤트 기간 여부
     */
    private boolean isPrizeDate() {
        LocalDateTime today = LocalDateTime.now();

        return today.minusDays(1).isAfter(dateEventStart) && today.plusDays(1).isBefore(dateEventEnd);
    }

    /**
     * 참가자가 일일 평균을 뛰어넘었는지 필터링
     *
     * @return 일일 평균 참가자 초과 여부
     */
    private boolean isGoalAverage() {
        LocalDateTime today = LocalDateTime.now();
        LocalDateTime todayStart = LocalDateTime.of(
                today.getYear(),
                today.getMonthValue(),
                today.getDayOfMonth(),
                0,
                0,
                0);
        LocalDateTime todayEnd = LocalDateTime.of(
                today.getYear(),
                today.getMonthValue(),
                today.getDayOfMonth(),
                23,
                59,
                59);

        long periodEvent = ChronoUnit.DAYS.between(dateEventStart, dateEventEnd) + 2;
        long average = (Ranking.FIRST.getPopulation()
                + Ranking.SECOND.getPopulation()
                + Ranking.THIRD.getPopulation()
                + Ranking.FIFTH.getPopulation())
                / periodEvent;

        return prizeRepository.countByDateCreateGreaterThanEqualAndDateCreateLessThanEqual(todayStart, todayEnd) > average;
    }

    /**
     * 등수 인원이 채워졌는지 필터링
     *
     * @param ranking 등수
     * @return 특정 등수 인원 채워졌는지 여부
     */
    private boolean isFullRanking(int ranking) {
        switch (ranking) {
            case 1 -> {
                return prizeRepository.countByRanking(ranking) >= Ranking.FIRST.getPopulation();
            }
            case 2 -> {
                return prizeRepository.countByRanking(ranking) >= Ranking.SECOND.getPopulation();
            }
            case 3 -> {
                return prizeRepository.countByRanking(ranking) >= Ranking.THIRD.getPopulation();
            }
            default -> {
                return false;
            }
        }
    }

    /**
     * 1등부터 3등까지 모두 채워졌는지 필터링
     *
     * @return 만석 여부
     */
    private boolean isFullRankingFirstBetweenThird() {
        return isFullRanking(1)
                && isFullRanking(2)
                && isFullRanking(3);
    }

    /**
     * 등수 뽑기
     *
     * @return 등수
     */
    private int drawPrize() {
        if(isGoalAverage() && isFullRankingFirstBetweenThird()) return 4;
        else if(isGoalAverage()) throw new CFullAverageException();

        int prizeRanking = 4;
        int rand = new Random()
                .nextInt(Ranking.FIRST.getPercentage()
                        + Ranking.SECOND.getPercentage()
                        + Ranking.THIRD.getPercentage()
                        + Ranking.FIFTH.getPercentage()) + 1;

        if(rand < Ranking.FIRST.getPercentage() + 1) {
            prizeRanking = 1;
        }
        else if(rand < Ranking.FIRST.getPercentage()
                + Ranking.SECOND.getPercentage()
                + 1) {
            prizeRanking = 2;
        }
        else if(rand < Ranking.FIRST.getPercentage()
                + Ranking.SECOND.getPercentage()
                + Ranking.THIRD.getPercentage()
                + 1) {
            prizeRanking = 3;
        }

        return confirmRanking(prizeRanking);
    }

    /**
     * 등수 확정짓기
     *
     * @param ranking 확률로 나온 등수
     * @return 확정 등수
     */
    private int confirmRanking(int ranking) {
        if (ranking == 3 && isFullRanking(3)) return confirmRanking(2);
        if (ranking == 2 && isFullRanking(2)) return confirmRanking(1);
        if (ranking == 1 && isFullRanking(1))  return 4;

        return ranking;
    }
}
