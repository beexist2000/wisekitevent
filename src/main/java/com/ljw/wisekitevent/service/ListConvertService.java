package com.ljw.wisekitevent.service;

import com.ljw.wisekitevent.model.common.ListResult;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListConvertService {
    /**
     * 페이지 번호를 인자로 받아서 인자로 받은 페이지와 기본으로 설정된 디폴트 사이즈만큼 받아서 반환
     *
     * @param pageNum 원하는 페이지 번호
     * @return 원하는 페이지
     */
    public static PageRequest getPageable(int pageNum) {
        return PageRequest.of(pageNum - 1, 10); // 원하는 페이지 반환, 컴퓨터는 숫자를 0부터 세기 때문에 페이지 번호에 -1
    }

    /**
     * 페이지 번호와 페이지 사이즈를 인자로 받아서 인자로 받은 페이지와 사이즈만큼으로 반환
     *
     * @param pageNum 원하는 페이지 번호
     * @param pageSize 원하는 페이지 사이즈
     * @return 원하는 페이지
     */
    public static PageRequest getPageable(int pageNum, int pageSize) {
        return PageRequest.of(pageNum - 1, pageSize); // 원하는 페이지 반환, 원하는 pageSize만큼 설정할 수 있다.
    }

    /**
     * List형 객체를 인자로 받아 ListResult형 객체 안의 멤버 변수들을 디폴트 값으로 초기화해주고 그 객체를 반환
     *
     * @param list 저장하고 싶은 리스트
     * @return ListResult 형식으로 저장하는 결과 값
     * @param <T> 데이터 타입
     */
    public static <T> ListResult<T> settingResult(List<T> list) {
        ListResult<T> result = new ListResult<>(); // ListResult형 객체 생성
        result.setList(list); // 객체에 list 저장
        result.setTotalItemCount((long)list.size()); // 객체에 list의 크기만큼 항목 수 저장
        result.setTotalPage(1); // 객체에 첫 페이지 등록
        result.setCurrentPage(1); // 객체에 현재 페이지 등록

        return result; // ListResult형 객체 생성
    }

    /**
     * List형 객체와 totalItemCount, totalPage, currentPage를 인자로 받아 ListResult형 객체 안의 멤버 변수들을 인자로 받은 값으로 초기화해주고 그 객체를 반환
     *
     * @param list 저장하고 싶은 리스트
     * @param totalItemCount 총 항목 수
     * @param totalPage 총 페이지 수
     * @param currentPage 현재 페이지 번호
     * @return ListResult 형식으로 저장하는 결과 값
     * @param <T> 데이터 타입
     */
    public static <T> ListResult<T> settingResult(List<T> list, long totalItemCount, int totalPage, int currentPage) {
        ListResult<T> result = new ListResult<>(); // ListResult형 객체 생성
        result.setList(list); // 객체에 인자 list 저장
        result.setTotalItemCount(totalItemCount); // 객체에 인자인 totalItemCount만큼 항목 수 저장
        result.setTotalPage(totalPage == 0 ? 1 : totalPage); // 객체에 인자인 totalPage가 0이면 1을 저장하고, 그렇지 않으면 인자인 totalPage만큼 저장한다
        result.setCurrentPage(currentPage + 1); // 객체에 인자인 currentPage에 +1 만큼 값을 저장한다.(컴퓨터에선 0페이지라도 사람에겐 1페이지로 보이도록)

        return result; // ListResult형 객체 반환
    }
}
