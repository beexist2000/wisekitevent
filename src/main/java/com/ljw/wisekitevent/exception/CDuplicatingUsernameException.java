package com.ljw.wisekitevent.exception;

public class CDuplicatingUsernameException extends RuntimeException {
    public CDuplicatingUsernameException(String msg, Throwable t) {
        super(msg, t); // 부모의 생성자에 msg, t 값을 인자로 주며 생성
    }

    public CDuplicatingUsernameException(String msg) {
        super(msg); // 부모의 생성자에 msg 값을 인자로 주며 생성
    }

    public CDuplicatingUsernameException() {
        super(); // 부모의 생성자를 생성
    }
}
