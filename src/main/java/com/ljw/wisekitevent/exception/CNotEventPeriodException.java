package com.ljw.wisekitevent.exception;

public class CNotEventPeriodException extends RuntimeException {
    public CNotEventPeriodException(String msg, Throwable t) {
        super(msg, t); // 부모의 생성자에 msg, t 값을 인자로 주며 생성
    }

    public CNotEventPeriodException(String msg) {
        super(msg); // 부모의 생성자에 msg 값을 인자로 주며 생성
    }

    public CNotEventPeriodException() {
        super(); // 부모의 생성자를 생성
    }
}
