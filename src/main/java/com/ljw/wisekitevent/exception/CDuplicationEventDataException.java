package com.ljw.wisekitevent.exception;

public class CDuplicationEventDataException extends RuntimeException {
    public CDuplicationEventDataException(String msg, Throwable t) {
        super(msg, t); // 부모의 생성자에 msg, t 값을 인자로 주며 생성
    }

    public CDuplicationEventDataException(String msg) {
        super(msg); // 부모의 생성자에 msg 값을 인자로 주며 생성
    }

    public CDuplicationEventDataException() {
        super(); // 부모의 생성자를 생성
    }
}
