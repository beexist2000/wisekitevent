package com.ljw.wisekitevent.exception;

public class CFullAverageException extends RuntimeException {
    public CFullAverageException(String msg, Throwable t) {
        super(msg, t); // 부모의 생성자에 msg, t 값을 인자로 주며 생성
    }

    public CFullAverageException(String msg) {
        super(msg); // 부모의 생성자에 msg 값을 인자로 주며 생성
    }

    public CFullAverageException() {
        super(); // 부모의 생성자를 생성
    }
}
