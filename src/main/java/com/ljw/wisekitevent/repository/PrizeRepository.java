package com.ljw.wisekitevent.repository;

import com.ljw.wisekitevent.entity.Member;
import com.ljw.wisekitevent.entity.Prize;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

public interface PrizeRepository extends JpaRepository<Prize, Long> {
    long countByDateCreateGreaterThanEqualAndDateCreateLessThanEqual(LocalDateTime dateStart, LocalDateTime dateEnd);
    long countByRanking(int ranking);
    long countByMember(Member member);

    Optional<Prize> findByMemberId(int memberId);
}
