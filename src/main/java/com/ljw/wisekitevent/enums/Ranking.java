package com.ljw.wisekitevent.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Ranking {
    FIRST(50, 1)
    , SECOND(250, 2)
    , THIRD(500, 4)
    , FIFTH(10000, 3)
    ;

    private final Integer population;
    private final Integer percentage;
}
