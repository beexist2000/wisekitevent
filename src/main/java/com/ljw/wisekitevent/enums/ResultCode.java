package com.ljw.wisekitevent.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    // 상황에 따른 코드와 메시지를 관리
    SUCCESS(0, "성공하였습니다.") // 성공 코드와 메시지
    , FAILED(-1, "실패하였습니다.") // 실패 코드와 메시지

    , MISSING_DATA(-10000, "데이터를 찾을 수 없습니다.") // 데이터를 찾을 수 없는 예외 상황에 대한 코드와 메시지
    , BAD_LOGIN(-10001, "아이디와 비밀번호를 확인해주세요.")

    , DUPLICATING_USERNAME(-20000, "유저 아이디가 중복되었습니다.")

    , NOT_EVENT_PERIOD(-30000, "이벤트 기간이 아닙니다.")
    , DUPLICATION_EVENT_DATA(-30001, "이미 이벤트에 참가하셨습니다.")
    , FULL_AVERAGE(-30002, "오늘 이벤트 참가는 마감되었습니다.")
    ;
    private final Integer code; // 코드의 자료 형식과 이름
    private final String msg; // 메시지의 자료 형식과 이름
}
