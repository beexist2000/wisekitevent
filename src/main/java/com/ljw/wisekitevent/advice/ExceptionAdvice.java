package com.ljw.wisekitevent.advice;

import com.ljw.wisekitevent.enums.ResultCode;
import com.ljw.wisekitevent.exception.*;
import com.ljw.wisekitevent.model.common.CommonResult;
import com.ljw.wisekitevent.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionAdvice {
    /**
     * 기본 탈출구 : 실패함
     *
     * @param e 예외 객체
     * @return ResponseService 클래스에 실패 정보 설정
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    /**
     * 커스텀 탈출구 : 데이터가 존재하지 않음
     *
     * @param e 예외 객체
     * @return ResponseService 클래스에 실패 정보 설정
     */
    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CDuplicatingUsernameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(CDuplicatingUsernameException e) {
        return ResponseService.getFailResult(ResultCode.DUPLICATING_USERNAME);
    }

    @ExceptionHandler(CBadLoginException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(CBadLoginException e) {
        return ResponseService.getFailResult(ResultCode.BAD_LOGIN);
    }

    @ExceptionHandler(CNotEventPeriodException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(CNotEventPeriodException e) {
        return ResponseService.getFailResult(ResultCode.NOT_EVENT_PERIOD);
    }

    @ExceptionHandler(CDuplicationEventDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(CDuplicationEventDataException e) {
        return ResponseService.getFailResult(ResultCode.DUPLICATION_EVENT_DATA);
    }

    @ExceptionHandler(CFullAverageException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(CFullAverageException e) {
        return ResponseService.getFailResult(ResultCode.FULL_AVERAGE);
    }
}
