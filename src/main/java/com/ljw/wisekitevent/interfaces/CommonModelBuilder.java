package com.ljw.wisekitevent.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
