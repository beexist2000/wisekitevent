package com.ljw.wisekitevent.model.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonResult {
    private Boolean isSuccess; // 성공 여부를 묻는 변수

    private Integer code; // isSuccess 결과에 따른 코드 값을 담는 변수

    private String msg; // isSuccess 결과에 따른 메세지를 담는 변수
}