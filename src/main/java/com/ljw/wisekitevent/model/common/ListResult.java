package com.ljw.wisekitevent.model.common;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListResult<T> extends CommonResult {
    private List<T> list; // 데이터 리스트를 저장할 변수(변수 타입은 나중에 선언할 때 정한다)

    private Long totalItemCount; // 리스트 항목 총 개수를 담을 변수

    private Integer totalPage; // 페이지 총 개수를 담을 변수

    private Integer currentPage; // 현재 페이지 번호를 담을 변수
}
