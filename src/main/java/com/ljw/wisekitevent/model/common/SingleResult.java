package com.ljw.wisekitevent.model.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SingleResult<T> extends CommonResult {
    private T data; // 데이터를 저장할 변수(변수 타입은 나중에 선언할 때 정한다)
}
