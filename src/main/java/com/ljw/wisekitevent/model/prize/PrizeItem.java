package com.ljw.wisekitevent.model.prize;

import com.ljw.wisekitevent.entity.Prize;
import com.ljw.wisekitevent.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PrizeItem {
    @Schema(description = "당첨 시퀀스")
    private Long prizeId;

    @Schema(description = "유저 시퀀스")
    private Long memberId;

    @Schema(description = "유저 ID")
    private String username;

    @Schema(description = "등수")
    private String ranking;

    @Schema(description = "데이터 생성일")
    private LocalDateTime dateCreate;

    private PrizeItem(PrizeItemBuilder builder) {
        this.prizeId = builder.prizeId;
        this.memberId = builder.memberId;
        this.username = builder.username;
        this.ranking = builder.ranking;
        this.dateCreate = builder.dateCreate;
    }

    public static class PrizeItemBuilder implements CommonModelBuilder<PrizeItem> {
        private final Long prizeId;
        private final Long memberId;
        private final String username;
        private final String ranking;
        private final LocalDateTime dateCreate;

        public PrizeItemBuilder(Prize prize) {
            this.prizeId = prize.getId();
            this.memberId = prize.getMember().getId();
            this.username = prize.getMember().getUsername();
            this.ranking = prize.getRanking() + "등";
            this.dateCreate = prize.getDateCreate();
        }

        @Override
        public PrizeItem build() {
            return new PrizeItem(this);
        }
    }
}
