package com.ljw.wisekitevent.model.prize;

import com.ljw.wisekitevent.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PrizeResponse {
    @Schema(description = "등수 메시지")
    private String msg;

    private PrizeResponse(PrizeResponseBuilder builder) {
        this.msg = builder.msg;
    }

    public static class PrizeResponseBuilder implements CommonModelBuilder<PrizeResponse> {
        private final String msg;

        public PrizeResponseBuilder(int ranking) {
            this.msg = "축하합니다. 당신은 " + ranking + "등에 당첨되었습니다.";
        }

        @Override
        public PrizeResponse build() {
            return new PrizeResponse(this);
        }
    }
}
