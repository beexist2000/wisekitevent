package com.ljw.wisekitevent.model.member;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class MemberRequest {
    @Schema(description = "유저 ID")
    @NotNull
    @Length(max = 20)
    private String username;

    @Schema(description = "유저 비밀번호")
    @NotNull
    @Length(max = 20)
    private String password;
}
