package com.ljw.wisekitevent.model.member;

import com.ljw.wisekitevent.entity.Member;
import com.ljw.wisekitevent.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {
    @Schema(description = "유저 시퀀스")
    private Long id;

    @Schema(description = "유저 ID")
    private String username;

    private MemberItem(MemberItemBuilder builder) {
        this.id = builder.id;
        this.username = builder.username;
    }

    public static class MemberItemBuilder implements CommonModelBuilder<MemberItem> {
        private final Long id;
        private final String username;

        public MemberItemBuilder(Member member) {
            this.id = member.getId();
            this.username = member.getUsername();
        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}
