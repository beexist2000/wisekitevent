package com.ljw.wisekitevent.model.member;

import com.ljw.wisekitevent.entity.Member;
import com.ljw.wisekitevent.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberLoginResponse {
    @Schema(description = "유저 시퀀스")
    private Long id;

    private MemberLoginResponse(MemberLoginResponseBuilder builder) {
        this.id = builder.id;
    }

    public static class MemberLoginResponseBuilder implements CommonModelBuilder<MemberLoginResponse> {
        private final Long id;

        public MemberLoginResponseBuilder(Member member) {
            this.id = member.getId();
        }

        @Override
        public MemberLoginResponse build() {
            return new MemberLoginResponse(this);
        }
    }
}
