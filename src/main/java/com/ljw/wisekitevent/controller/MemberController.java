package com.ljw.wisekitevent.controller;

import com.ljw.wisekitevent.model.common.CommonResult;
import com.ljw.wisekitevent.model.common.ListResult;
import com.ljw.wisekitevent.model.common.SingleResult;
import com.ljw.wisekitevent.model.member.MemberItem;
import com.ljw.wisekitevent.model.member.MemberLoginResponse;
import com.ljw.wisekitevent.model.member.MemberRequest;
import com.ljw.wisekitevent.service.ResponseService;
import com.ljw.wisekitevent.service.MemberService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Tag(name = "유저 관리")
@RestController
@RequestMapping("/v1/user")
@RequiredArgsConstructor
public class MemberController {
    private final MemberService memberService;

    @Operation(summary = "로그인")
    @PostMapping("/login")
    public SingleResult<MemberLoginResponse> login(@RequestBody @Valid MemberRequest request) {
        return ResponseService.getSingleResult(memberService.login(request));
    }

    @Operation(summary = "유저 생성")
    @PostMapping("/new")
    public CommonResult setUser(@RequestBody @Valid MemberRequest request) {
        memberService.setMember(request);

        return ResponseService.getSuccessResult();
    }

    @Operation(summary = "유저 리스트 조회")
    @GetMapping("/all")
    public ListResult<MemberItem> getMembers() {
        return ResponseService.getListResult(memberService.getMembers(), true);
    }

    @Operation(summary = "유저 조회")
    @Parameters({
            @Parameter(name = "memberId", description = "유저 시퀀스")
    })
    @GetMapping("/memberId/{memberId}")
    public SingleResult<MemberItem> getMember(@PathVariable long memberId) {
        return ResponseService.getSingleResult(memberService.getMember(memberId));
    }
}
