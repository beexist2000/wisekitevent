package com.ljw.wisekitevent.controller;

import com.ljw.wisekitevent.entity.Member;
import com.ljw.wisekitevent.model.common.ListResult;
import com.ljw.wisekitevent.model.common.SingleResult;
import com.ljw.wisekitevent.model.prize.PrizeItem;
import com.ljw.wisekitevent.model.prize.PrizeResponse;
import com.ljw.wisekitevent.service.MemberService;
import com.ljw.wisekitevent.service.PrizeService;
import com.ljw.wisekitevent.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Tag(name = "당첨 이벤트 관리")
@RestController
@RequestMapping("/v1/prize")
@RequiredArgsConstructor
public class PrizeController {
    private final MemberService memberService;
    private final PrizeService prizeService;

    @PostMapping("/new/memberId/{memberId}")
    @Operation(summary = "이벤트 참가")
    @Parameters({
            @Parameter(name = "memberId", description = "유저 시퀀스")
    })
    public SingleResult<PrizeResponse> setPrize(@PathVariable int memberId) {
        Member member = memberService.getMemberFull(memberId);
        return ResponseService.getSingleResult(prizeService.setPrize(member));
    }

    @Operation(summary = "참여 데이터 리스트 조회")
    @GetMapping("/all")
    public ListResult<PrizeItem> getPrizes() {
        return ResponseService.getListResult(prizeService.getPrizes(), true);
    }

    @Operation(summary = "참여 데이터 조회")
    @Parameters({
            @Parameter(name = "memberId", description = "유저 시퀀스")
    })
    @GetMapping("/memberId/{memberId}")
    public SingleResult<PrizeItem> getPrizes(@PathVariable int memberId) {
        return ResponseService.getSingleResult(prizeService.getPrize(memberId));
    }
}
