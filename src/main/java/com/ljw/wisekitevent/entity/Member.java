package com.ljw.wisekitevent.entity;

import com.ljw.wisekitevent.interfaces.CommonModelBuilder;
import com.ljw.wisekitevent.model.member.MemberRequest;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {
    @Schema(description = "유저 시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Schema(description = "유저 ID")
    @Column(length = 20, nullable = false, unique = true)
    private String username;

    @Schema(description = "유저 비밀번호")
    @Column(nullable = false)
    private String password;

    @Schema(description = "유저 생성일")
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    private Member(UserBuilder builder) {
        this.username = builder.username;
        this.password = builder.password;
        this.dateCreate = builder.dateCreate;
    }

    public static class UserBuilder implements CommonModelBuilder<Member> {
        private final String username;
        private final String password;
        private final LocalDateTime dateCreate;

        public UserBuilder(MemberRequest request) {
            this.username = request.getUsername();
            this.password = request.getPassword();
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
