package com.ljw.wisekitevent.entity;

import com.ljw.wisekitevent.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Prize {
    @Schema(description = "당첨 시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Schema(description = "유저 시퀀스")
    @JoinColumn(name = "memberId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Member member;

    @Schema(description = "당첨 순위")
    @Column(nullable = false)
    private Integer ranking;

    @Schema(description = "데이터 생성일")
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    private Prize(PrizeBuilder builder) {
        this.member = builder.member;
        this.ranking = builder.ranking;
        this.dateCreate = builder.dateCreate;
    }

    public static class PrizeBuilder implements CommonModelBuilder<Prize> {
        private final Member member;
        private final Integer ranking;
        private final LocalDateTime dateCreate;

        public PrizeBuilder(Member member, int ranking) {
            this.member = member;
            this.ranking = ranking;
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public Prize build() {
            return new Prize(this);
        }
    }
}
